﻿(function($) {
    "use strict";

    jQuery(document).ready(function($) {
        $(window).load(function() {
            $("#sticker").sticky({ topSpacing: 0 });
        });

        $(".navbar-toggle").click(function() {
            $("body").addClass("mobil-menu-activted");
        });

        $("#countdown-3").timeTo({
            timeTo: new Date("Dec 01 2017"),
            displayDays: 1,
            theme: "black",
            displayCaptions: true,
            fontSize: 48,
            captionSize: 14
        });

        $.get("https://icoapi.plaak.com/v1/ico/total/eth", function(data) {
            $("#etherium-raised").html(data);
        });
        $.get("https://icoapi.plaak.com/v1/ico/total/btc", function(data) {
            $("#bitcoin-raised").html(data);
        });
        $.get("https://icoapi.plaak.com/v1/ico/total/sold", function(data) {
            var soldTokens = parseInt(data, 10);
            $("#tokens-sold").html(soldTokens.toLocaleString("en-US"));
            var filledPercentage = (soldTokens / targetFunding * 100).toFixed(
                2
            );
            var emptyPercentage = 100 - filledPercentage;
            $("#filled-percentage").html(filledPercentage);
            $("#empty-percentage").html("(" + emptyPercentage + "%)");
            $("#progress-bar").css("width", filledPercentage + "%");
        });
        $.get("https://icoapi.plaak.com/v1/ico/total/usd", function(data) {
            $("#usd-raised").html("$" + parseInt(data, 10).toFixed(2));
        });


        $("#contributor-form-submit").on("click", function() {
            if (check) {
                sendinfo();
            }
        });

        $("#contribution-address").on("click", copyToClipboard);

        // Skills-graph
        if ($("#skills").length > 0) {
            var ctx = document.getElementById("skills");
            var Skills = new Chart(ctx, {
                type: "pie",
                data: {
                    labels: $("#skills").data("label"),
                    datasets: [
                        {
                            data: [70, 12, 3, 10, 5],
                            backgroundColor: [
                                "#4F81BD",
                                "#C0504D",
                                "#9BBB59",
                                "#8064A2",
                                "#4BACC6"
                            ],
                            hoverBackgroundColor: [
                                "#638cbe",
                                "#bf5e5b",
                                "#a1bb6a",
                                "#8772a1",
                                "#62b2c7"
                            ]
                        }
                    ]
                }
            });
        }
        // Skills-graph
        if ($("#skills2").length > 0) {
            var ctx = document.getElementById("skills2");
            var Skills = new Chart(ctx, {
                type: "pie",
                data: {
                    labels: $("#skills2").data("label"),
                    datasets: [
                        {
                            data: [78, 12, 10],
                            backgroundColor: ["#4F81BD", "#C0504D", "#9BBB59"],
                            hoverBackgroundColor: [
                                "#638cbe",
                                "#bf5e5b",
                                "#a1bb6a"
                            ]
                        }
                    ]
                }
            });
        }

        $("#language-switcher").click(function() {
            $("#language-switcher-list").toggle();
        });

        // scroll top
        $("#scroll").click(function() {
            $("html,body").animate({ scrollTop: 0 }, 1000);
        });

        var plkWalletCookie = Cookies.get('plk_wallet');

        if(plkWalletCookie) {
          getWalletBalance(plkWalletCookie);
        } else {
          $('#checkwalletlink').css({ display: 'inline-block'});
        }

        $("#checkwalletlink").click(function() {
          $(this).hide();
          $("#savewalletaddr").css({ display: 'inline-block'});
        });

        $('#btnSaveWalletAddress').on('click', function(e) {
          e.preventDefault();
          var walletaddr = $('#inputSuccess4').val();
          Cookies.set('plk_wallet', walletaddr,9999);

          getWalletBalance(walletaddr);

        });

        $('#editaddr').click(function() {
          $('#myPlkWallet').hide();
          $("#savewalletaddr").css({ display: 'inline-block'});
        })

        function getWalletBalance(walletaddr) {
          $.getJSON("https://icoapi.plaak.com/v1/plk/balance?addr=" + walletaddr, function(result){
            var balance = parseFloat(result.balance).toFixed(2);
             $("#plkbalance").html(balance);
             $("#savewalletaddr").hide();
             $("#myPlkWallet").css({ 'display' : 'inline-block' });
          });
        }
    });

    $(document).ready(function() {
        // Build the chart
        Highcharts.chart("container", {
            chart: {
                type: "pie"
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: "pointer",
                    dataLabels: {
                        enabled: false,
                        distance: -50,
                    },
                    showInLegend: true
                }
            },
            series: [
                {
                    data: [
                        {
                            y: 70
                        },
                        {
                            y: 12
                        },
                        {
                            y: 3
                        },
                        {
                            y: 10
                        },
                        {   name: 'Technology Imporvements',
                            y: 5
                        }
                    ]
                }
            ]
        });
    });
    $(document).ready(function() {
        // Build the chart
        Highcharts.chart("container2", {
            chart: {
                type: "pie"
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: "pointer",
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [
                {
                    data: [ { y: 78},{y: 12},{ y: 10}]
                }
            ]
        });
    });
})(jQuery);

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        $("#scroll").fadeIn();
    } else {
        $("#scroll").fadeOut();
    }
    return false;
});

function validatePhone(phone) {
    return String(phone).match(/^[0-9]*$/);
}

function validateName(name) {
    return  name.length > 2 &&    String(name).match(/^[a-zA-Z]+$/);
}

function validateAddr(addr) {
    if (addr.substring(0, 2) != "0x") {
        addr = "0x" + addr;
    }
    var re = /^0x[a­fA­F0­9]{40}$/;
    return re.test(addr) || addr.length == 42;
}

function copyToClipboard() {

    console.log('copy');
    $("#contribution-address-input").select();
    document.execCommand("copy");
    //CopyToClipboard('#contribution-address');
    //console.log('id:' + document.getElementById(containerid));
}

function CopyToClipboardx(containerid) {
if (document.selection) {
    var range = document.body.createTextRange();
    range.moveToElementText(document.getElementById(containerid));
    range.select().createTextRange();
    document.execCommand("copy");

} else if (window.getSelection) {
    var range = document.createRange();
     range.selectNode(document.getElementById(containerid));
     window.getSelection().addRange(range);
     document.execCommand("copy");
     alert("text copied")
}}
