﻿host = "https://icoapi.plaak.com/v1/";
conttype = "ETH";

var targetFunding = 500;

$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $(".step").hide();
    $("#step0").show();
    //$(".named").on('change',validform);
});
ethcontribution = function() {
    conttype = "ETH";
    $("#contribution-type").val("eth");
    showstep(1);
    $.post(host + "ico/contract", function(data) {
        $("#contribution-address").html(data);
    });
    $('#cont-type').html(conttype);
};

btccontribution = function() {
    $('.eth-spec').hide();
    conttype = "BTC";
    $("#contribution-type").val("btc");
    showstep(1);
    $('#cont-type').html(conttype);
};

var showstep = function(step) {
    $(".step").hide();
    $("#step" + step).show();
};

var sendinfo = function() {
    showstep(5);
};
namedcont = function() {
    if(!validform()){
        return;
    }

    if (!validateAddr($("#addr").val()) && conttype === "BTC") {
        $("#addr-err").html("Please enter valid address");
        return;
    }
    data = {
        type: conttype,
        addr: $("#addr").val(),
        firstname: $("#firstname").val(),
        lastname: $("#lastname").val(),
        phone: $("#phone").val(),
        email: $("#email").val()
    };

    $(".step").hide();
    $("#loading").show();
    $("#loading-text").html("Generating your address");
    $.ajax({
        type: "POST",
        url: host + "sale/contribute",
        data: data,
        success: function(data) {
            console.log(data);
            showstep(5);
            $("#contribution-address").html(data);
            document.execCommand("copy");
            var qrcode = new QRCode("qrcode", {
                text: data,
                width: 256,
                height: 256,
                colorDark: "#000000",
                colorLight: "#ffffff",
                correctLevel: QRCode.CorrectLevel.H
            });
        },
        error: function(data) {
            $(".step").hide();
            $("#no-service").show();
        }
    });

}
anonymously = function() {
    $('.named').hide();
    if (!validateAddr($("#addr").val()) && conttype === "BTC") {
        $("#addr-err").html("Please enter valid address");
        return;
    }
    data = {
        addr: $("#addr").val(),
	type:conttype,
        firstname: $("#firstname").val(),
        lastname: $("#lastname").val(),
        phone: $("#phone").val(),
        email: $("#email").val()
    };

    $(".step").hide();
    $("#loading").show();
    $("#loading-text").html("Generating your address");
    $.ajax({
        type: "POST",
        url: host + "sale/contribute",
        data: data,
        success: function(data) {
            console.log(data);
            showstep(5);
            $("#contribution-address").html(data);
            document.execCommand("copy");
            var qrcode = new QRCode("qrcode", {
                text: data,
                width: 256,
                height: 256,
                colorDark: "#000000",
                colorLight: "#ffffff",
                correctLevel: QRCode.CorrectLevel.H
            });
        },
        error: function(data) {
            $(".step").hide();
            $("#no-service").show();
        }
    });
};

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validateAddr(addr) {
    if (addr.substring(0, 2) != "0x") {
        addr = "0x" + addr;
    }
    var re = /^0x[a­fA­F0­9]{40}$/;
    return re.test(addr) || addr.length == 42;
}

// Validation
var contributorForm = $("#contributor-form")[0];
var check = false;


function validform() {
    var firstname = contributorForm.elements.firstname.value;
    var lastname = contributorForm.elements.lastname.value;
    var phone = contributorForm.elements.phone.value;
    var email = contributorForm.elements.email.value;

    check = true;
    if (!validateName(firstname) ) {
        $("#firstname-err").html("Please enter a valid name");
        console.log("Please enter a valid name");
        check = false;
    } else {
        $("#firstname-err").html("");
    }
    if (!validateName(lastname) ) {
        $("#lastname-err").html("Please enter a valid name");
        console.log("Please enter a valid name");
        check = false;
    } else {
        $("#lastname-err").html("");
    }
    if (!validateEmail(email) ) {
        $("#email-err").html("Please enter a valid email");
        console.log("Please enter a valid email");
        check = false;
    } else {
        $("#email-err").html("");
    }
    if (!validatePhone(phone) && phone !== "") {
        $("#phone-err").html("Please enter a valid phone number");
        console.log("Please enter a valid phone number");
        check = false;
    } else {
        $("#phone-err").html("");
    }
    if (!validateAddr($("#addr").val()) ) {
        $("#addr-err").html("Please enter valid address");
        console.log("Please enter valid address");
        check = false;
    } else {
        $("#addr-err").html("");
    }
    return check;
}

